//
//  DCAMAppDelegate.m
//  Basic lib1394
//
//  Created by William Dillon on 7/9/12.
//  Copyright (c) 2012 William Dillon. All rights reserved.
//

#import "DCAMAppDelegate.h"
#import "DCAMDevice.h"
#import "DCAMFrameView.h"

@implementation DCAMAppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    _selectedDevice = -1;
    
    deviceList = [DCAMDevice deviceList];
    // If device list is nil, there was a failure in the 1394 system
    if (deviceList == nil) {
        // Display an error and close
        NSAlert *alert = [NSAlert alertWithMessageText:@"Unable to open 1394"
                                         defaultButton:@"Close"
                                       alternateButton:nil
                                           otherButton:nil
                             informativeTextWithFormat:@"I was unable to initialize the dc1394 library."];
        
        // Wait for the user to click it
        [alert runModal];
        
        // Shut down the app
        NSApplication *app = [NSApplication sharedApplication];
        [app stop:self];
        return;
    }
    
    // If the device list is empty there are no cameras
    if ([deviceList count] == 0) {
        // Display an error and close
        NSAlert *alert = [NSAlert alertWithMessageText:@"No device found"
                                         defaultButton:@"Close"
                                       alternateButton:nil
                                           otherButton:nil
                             informativeTextWithFormat:@"No firewire camera were found."];
        
        // Wait for the user to click it
        [alert runModal];
        
        // Shut down the app
        NSApplication *app = [NSApplication sharedApplication];
        [app stop:self];
        return;
    }
    
    // Default to the first device
    [self setSelectedDevice:0];
    [device start];
    
}

-(NSApplicationTerminateReply)applicationShouldTerminate:(NSApplication *)sender
{
    // Make sure that the camer is shut down
    [device stop];
    device = nil;
    
    return YES;
}

- (int)selectedDevice { return _selectedDevice; };
- (void)setSelectedDevice:(int)selectedDevice
{
    // If the selected device didn't change, do nothing
    if (_selectedDevice == selectedDevice) {
        return;
    }
    
    DCAMDevice *newDevice = [DCAMDevice newDeviceWithID:selectedDevice];
    if (newDevice == nil) {
        // Display an error and close
        NSAlert *alert = [NSAlert alertWithMessageText:@"Unable to open device"
                                         defaultButton:@"Close"
                                       alternateButton:nil
                                           otherButton:nil
                             informativeTextWithFormat:@"Unable to open the new device."];
        
        // Wait for the user to click it
        [alert runModal];
        
        // Do nothing (keep the first device)
        
    }

    _selectedDevice = selectedDevice;
    device = newDevice;
    
    // Query the device for available features (adjustable gain, exposure, etc.)
    
    if ([device gainFeature]) {
        [[self gainSlider] setEnabled:YES];
        [[self gainSlider] setMinValue:[device minGain]];
        [[self gainSlider] setMaxValue:[device maxGain]];
        
        gain = [device gain];
        [[self gainSlider] setFloatValue:gain];
    } else {
        [[self gainSlider] setEnabled:NO];
    }
    
    if ([device exposureFeature]) {
        [[self exposureSlider] setEnabled:YES];
        [[self exposureSlider] setMinValue:[device minExposure]];
        [[self exposureSlider] setMaxValue:[device maxExposure]];

        exposure = [device exposure];
        [[self exposureSlider] setFloatValue:exposure];
    } else {
        [[self exposureSlider] setEnabled:NO];
    }

    if ([device shutterFeature]) {
        [[self shutterSlider] setEnabled:YES];
        [[self shutterSlider] setMinValue:[device minShutter]];
        [[self shutterSlider] setMaxValue:[device maxShutter]];
        
        shutter = [device shutter];
        [[self shutterSlider] setFloatValue:shutter];
    } else {
        [[self shutterSlider] setEnabled:NO];
    }
}


- (float)gain { return gain; }
- (void)setGain:(float)newGain {
    [device setGain:newGain];
    gain = [device gain];
}

- (float)exposure { return exposure; }
- (void)setExposure:(float)newExposure {
    [device setExposure:newExposure];
    exposure = [device exposure];
}

- (float)shutter { return shutter; }
- (void)setShutter:(float)newShutter {
    [device setShutter:newShutter];
    shutter = [device shutter];
}

- (void)printFeatures:(id)sender
{
    [device printFeatures];
}

@end
