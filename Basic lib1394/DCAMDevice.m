//
//  DCAMDevice.m
//  Basic lib1394
//
//  Created by William Dillon on 7/9/12.
//  Copyright (c) 2012 William Dillon. All rights reserved.
//

#import "DCAMDevice.h"

NSString *DCAMCapturedFrameNotification = @"DCAMCapturedFrameNotification";

/*-----------------------------------------------------------------------
 *  Prints the type of format to standard out
 *-----------------------------------------------------------------------*/
void print_format( uint32_t format )
{
#define print_case(A) case A: printf(#A ""); break;
    
    switch( format ) {
            print_case(DC1394_VIDEO_MODE_160x120_YUV444);
            print_case(DC1394_VIDEO_MODE_320x240_YUV422);
            print_case(DC1394_VIDEO_MODE_640x480_YUV411);
            print_case(DC1394_VIDEO_MODE_640x480_YUV422);
            print_case(DC1394_VIDEO_MODE_640x480_RGB8);
            print_case(DC1394_VIDEO_MODE_640x480_MONO8);
            print_case(DC1394_VIDEO_MODE_640x480_MONO16);
            print_case(DC1394_VIDEO_MODE_800x600_YUV422);
            print_case(DC1394_VIDEO_MODE_800x600_RGB8);
            print_case(DC1394_VIDEO_MODE_800x600_MONO8);
            print_case(DC1394_VIDEO_MODE_1024x768_YUV422);
            print_case(DC1394_VIDEO_MODE_1024x768_RGB8);
            print_case(DC1394_VIDEO_MODE_1024x768_MONO8);
            print_case(DC1394_VIDEO_MODE_800x600_MONO16);
            print_case(DC1394_VIDEO_MODE_1024x768_MONO16);
            print_case(DC1394_VIDEO_MODE_1280x960_YUV422);
            print_case(DC1394_VIDEO_MODE_1280x960_RGB8);
            print_case(DC1394_VIDEO_MODE_1280x960_MONO8);
            print_case(DC1394_VIDEO_MODE_1600x1200_YUV422);
            print_case(DC1394_VIDEO_MODE_1600x1200_RGB8);
            print_case(DC1394_VIDEO_MODE_1600x1200_MONO8);
            print_case(DC1394_VIDEO_MODE_1280x960_MONO16);
            print_case(DC1394_VIDEO_MODE_1600x1200_MONO16);
            
        default:
            dc1394_log_error("Unknown format: %d\n", format);
            exit(1);
    }
    
}

/*-----------------------------------------------------------------------
 *  Returns the number of pixels in the image based upon the format
 *-----------------------------------------------------------------------*/
uint32_t get_num_pixels(dc1394camera_t *camera, uint32_t format ) {
    uint32_t w,h;
    
    dc1394_get_image_size_from_video_mode(camera, format,&w,&h);
    
    return w*h;
}

/*-----------------------------------------------------------------------
 *  Prints the type of color encoding
 *-----------------------------------------------------------------------*/
void print_color_coding( uint32_t color_id )
{
    switch( color_id ) {
        case DC1394_COLOR_CODING_MONO8:
            printf("MONO8");
            break;
        case DC1394_COLOR_CODING_YUV411:
            printf("YUV411");
            break;
        case DC1394_COLOR_CODING_YUV422:
            printf("YUV422");
            break;
        case DC1394_COLOR_CODING_YUV444:
            printf("YUV444");
            break;
        case DC1394_COLOR_CODING_RGB8:
            printf("RGB8");
            break;
        case DC1394_COLOR_CODING_MONO16:
            printf("MONO16");
            break;
        case DC1394_COLOR_CODING_RGB16:
            printf("RGB16");
            break;
        case DC1394_COLOR_CODING_MONO16S:
            printf("MONO16S");
            break;
        case DC1394_COLOR_CODING_RGB16S:
            printf("RGB16S");
            break;
        case DC1394_COLOR_CODING_RAW8:
            printf("RAW8");
            break;
        case DC1394_COLOR_CODING_RAW16:
            printf("RAW16");
            break;
        default:
            dc1394_log_error("Unknown color coding = %d\n",color_id);
            exit(1);
    }
}

/*-----------------------------------------------------------------------
 *  Prints various information about the mode the camera is in
 *-----------------------------------------------------------------------*/
void print_mode_info( dc1394camera_t *camera , uint32_t mode )
{
    int j;
    
    printf("Mode: ");
    print_format(mode);
    printf("\n");
    
    dc1394framerates_t framerates;
    dc1394error_t err;
    err=dc1394_video_get_supported_framerates(camera,mode,&framerates);
    DC1394_ERR(err,"Could not get frame rates");
    
    printf("Frame Rates:\n");
    for( j = 0; j < framerates.num; j++ ) {
        uint32_t rate = framerates.framerates[j];
        float f_rate;
        dc1394_framerate_as_float(rate,&f_rate);
        printf("  [%d] rate = %f\n",j,f_rate );
    }
    
}

@implementation DCAMDevice

static dc1394_t *systemDevice;
static NSArray *deviceList;
static dispatch_once_t onceToken;
static dc1394camera_list_t *list;

+(NSInteger)deviceCount
{
    return [[DCAMDevice deviceList] count];
}

+(NSArray *)deviceList
{

    // Enumerate devices once at the start, after just return the array.
    dispatch_once(&onceToken, ^{
        dc1394error_t retval;
        NSMutableArray *tempDeviceList = [[NSMutableArray alloc] init];
        
        systemDevice = dc1394_new();
        if (systemDevice == nil) {
            NSLog(@"Unable to create 1394 interface.");
            return;
        }

        retval = dc1394_camera_enumerate(systemDevice, &list);
        if (retval != DC1394_SUCCESS) {
            NSLog(@"Unable to enumerate cameras: %d", retval);
            return;
        }
        
        for (int i = 0; i < list->num; i++) {
            NSLog(@"Found camera, GUID: %"PRIx64"\n", list->ids[0].guid);
            
            // Add a dictionary entry for the camera
            [tempDeviceList addObject:@{
             @"GUID" : [NSString stringWithFormat:@"%"PRIx64"", list->ids[0].guid],
             @"ID"   : @(i)}];
        }
        
        deviceList = tempDeviceList;
    });
    
    return deviceList;
}

- (id)initWithID:(int)index
{
    self = [super init];
    if (self) {
        dc1394error_t retval;

        // Get the device from the list (make sure it's enumerated first)
        [DCAMDevice deviceList];
        
        // Make sure that the new deviceID is within the bounds
        if(index >= [deviceList count]) {
            NSLog(@"Attempted access to a device that doesn't exist (out of bounds)");
            return nil;
        }
        
        // Create a new camera object
        camera = dc1394_camera_new(systemDevice, list->ids[index].guid);
        if (!camera) {
            NSLog(@"Unable to initialize camera");
            return nil;
        }

        // I think this is isochronous transfer speed, not film speed (!)
        retval = dc1394_video_set_iso_speed(camera, DC1394_ISO_SPEED_400);
        if (retval != DC1394_SUCCESS) {
            NSLog(@"Unable to set isochronous transfer speed: %d", retval);
        }

        // Get 1024x768 mono images
        retval = dc1394_video_set_mode(camera, DC1394_VIDEO_MODE_1024x768_MONO8);
        if (retval != DC1394_SUCCESS) {
            NSLog(@"Unable to set camera mode: %d", retval);
        }

        // Set the framerate
        retval = dc1394_video_set_framerate(camera, DC1394_FRAMERATE_15);
        if (retval != DC1394_SUCCESS) {
            NSLog(@"Unable to set framerate: %d", retval);
        }

        [self getFeatures];
    }
    
    NSLog(@"Successfully opened camera!");
    
    return self;
}

- (void)getFeatures
{
    dc1394feature_info_t feature;
    
    feature.id = DC1394_FEATURE_GAIN;
    dc1394_feature_get(camera, &feature);
    if (feature.available) {
        _gainFeature = YES;
        _maxGain = feature.max;
        _minGain = feature.min;
        _gain   = feature.value;
    }

    feature.id = DC1394_FEATURE_BRIGHTNESS;
    dc1394_feature_get(camera, &feature);
    if (feature.available) {
        _brightnessFeature = YES;
        _maxBrightness = feature.max;
        _minBrightness = feature.min;
        _brightness   = feature.value;
    }
    
    feature.id = DC1394_FEATURE_SHUTTER;
    dc1394_feature_get(camera, &feature);
    if (feature.available) {
        _shutterFeature = YES;
        _maxShutter = feature.max;
        _minShutter = feature.min;
        _shutter   = feature.value;
    }

    feature.id = DC1394_FEATURE_EXPOSURE;
    dc1394_feature_get(camera, &feature);
    if (feature.available) {
        _exposureFeature = YES;
        _maxExposure = feature.max;
        _minExposure = feature.min;
        _exposure   = feature.value;
    }

}

+ (DCAMDevice *)newDeviceWithID:(int)index
{
    return [[DCAMDevice alloc] initWithID:index];
}

- (void)frameReadThread
{
    int errors = 0;
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    dc1394error_t retval;

    do {
        // Get a frame from the camera
        dc1394video_frame_t *frame = nil;
        retval = dc1394_capture_dequeue(camera, DC1394_CAPTURE_POLICY_WAIT, &frame);
        // The frame memory is owned by the system, do not free it.
        
        if (retval != DC1394_SUCCESS) {
            
            if (!running) {
                return;
            }
            
            errors += 1;
            
            if (errors < 100) {
                NSLog(@"Error capturing frame.");
                continue;
            } else {
                NSLog(@"Too many frame errors.");
                return;
            }
        }
        
        width  = frame->size[0];
        height = frame->size[1];

        // Convert to RGB
        dc1394video_frame_t *newFrame = calloc(1, sizeof(dc1394video_frame_t));
        frame->color_filter = DC1394_COLOR_FILTER_BGGR;
        frame->color_coding = DC1394_COLOR_CODING_MONO8;
        newFrame->color_coding=DC1394_COLOR_CODING_RGB8;

        dc1394_debayer_frames(frame, newFrame, DC1394_BAYER_METHOD_NEAREST);
        
        // Compute the new frame size
        size_t frameSize = width * height * 3;
        
        // Encapsulate the data into an NSData
        NSData *tempData = [NSData dataWithBytes:newFrame->image length:frameSize];
        
        // free the new frame
        free(newFrame->image);
        free(newFrame);

        dc1394_capture_enqueue(camera, frame);
        frame = nil;
        
        // Send a notification with the new data on another thread
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
        ^{
            [center postNotificationName:DCAMCapturedFrameNotification object:tempData];
        });

    } while (running);

    return;
}

// Start isochronous transmission of frames from the camera
- (bool)start
{
    dc1394error_t retval;

    // Setup the camera
    retval = dc1394_capture_setup(camera, 4, DC1394_CAPTURE_FLAGS_DEFAULT);
    if (retval != DC1394_SUCCESS) {
        NSLog(@"Unable to setup camera: %d", retval);
        return nil;
    }

    retval = dc1394_video_set_transmission(camera, DC1394_ON);
    if (retval != DC1394_SUCCESS) {
        NSLog(@"Unable to start capturing frames: %d", retval);
        return NO;
    }
    
    if (FIFO_read_queue == nil) {
        FIFO_read_queue = dispatch_queue_create("com.us.alternet.dcam", DISPATCH_QUEUE_SERIAL);
    }
    
    // Start a read thread
    dispatch_async(FIFO_read_queue,
    ^{
        running = YES;
        [self frameReadThread];
    });
    
    NSLog(@"Successfully started camera transfers");
    
    return YES;
}

// Stop isochronous transmission of frames
- (bool)stop
{
    // Shut down the transfers
    dc1394error_t retval;
    
    retval = dc1394_video_set_transmission(camera, DC1394_OFF);
    if (retval != DC1394_SUCCESS) {
        NSLog(@"Unable to start capturing frames: %d", retval);
        return NO;
    }
    
    NSLog(@"Successfully stop camera transfers");
    
    // Stop the read thread
    running = NO;
//    dispatch_sync(FIFO_read_queue,
//    ^{
//        NSLog(@"Successfully halted read thread.");
//    });
    
    return YES;
}

// In the dealloc method, close the camera
- (void)dealloc
{
    [self stop];
    dc1394_capture_stop(camera);
    dc1394_camera_free(camera);
}

- (void)printFeatures
{
    // Get a list of features
    dc1394featureset_t features;
    dc1394_feature_get_all(camera, &features);
    dc1394_feature_print_all(&features, stdout);
}

#pragma mark mutators
- (float)gain
{
    uint32_t value;
    dc1394_feature_get_value(camera, DC1394_FEATURE_GAIN, &value);
    
    _gain   = (float)value;
    
    return _gain;
}

- (void)setGain:(float)gain
{
    dc1394feature_mode_t mode;
    dc1394_feature_get_mode(camera, DC1394_FEATURE_GAIN, &mode);

    // Check to make sure the input is within bounds
    // if it isn't, set to automatic mode (if it isn't already)
    if (gain < _minGain ||
        gain > _maxGain) {
        if (mode == DC1394_FEATURE_MODE_MANUAL) {
            dc1394_feature_set_mode(camera, DC1394_FEATURE_GAIN, DC1394_FEATURE_MODE_AUTO);
        }
        return;
    }
    
    // It's within range, set to manual if its not already
    if (mode == DC1394_FEATURE_MODE_AUTO) {
        dc1394_feature_set_mode(camera, DC1394_FEATURE_GAIN, DC1394_FEATURE_MODE_MANUAL);
    }

    // Set the value
    dc1394_feature_set_value(camera, DC1394_FEATURE_GAIN, gain);
}

- (float)exposure
{
    uint32_t value;
    dc1394_feature_get_value(camera, DC1394_FEATURE_EXPOSURE, &value);
    
    _exposure = (float)value;
    
    return _exposure;
}

- (void)setExposure:(float)exposure
{
    dc1394feature_mode_t mode;
    dc1394_feature_get_mode(camera, DC1394_FEATURE_EXPOSURE, &mode);
    
    // Check to make sure the input is within bounds
    // if it isn't, set to automatic mode (if it isn't already)
    if (exposure < _minExposure ||
        exposure > _maxExposure) {
        if (mode == DC1394_FEATURE_MODE_MANUAL) {
            dc1394_feature_set_mode(camera, DC1394_FEATURE_EXPOSURE, DC1394_FEATURE_MODE_AUTO);
        }
        return;
    }
    
    // It's within range, set to manual if its not already
    if (mode == DC1394_FEATURE_MODE_AUTO) {
        dc1394_feature_set_mode(camera, DC1394_FEATURE_EXPOSURE, DC1394_FEATURE_MODE_MANUAL);
    }
    
    // Set the value
    dc1394_feature_set_value(camera, DC1394_FEATURE_EXPOSURE, exposure);
}

- (float)shutter
{
    uint32_t value;
    dc1394_feature_get_value(camera, DC1394_FEATURE_SHUTTER, &value);
    
    _exposure = (float)value;
    
    return _exposure;
}

- (void)setShutter:(float)shutter
{
    dc1394feature_mode_t mode;
    dc1394_feature_get_mode(camera, DC1394_FEATURE_SHUTTER, &mode);
    
    // Check to make sure the input is within bounds
    // if it isn't, set to automatic mode (if it isn't already)
    if (shutter < _minShutter ||
        shutter > _maxShutter) {
        if (mode == DC1394_FEATURE_MODE_MANUAL) {
            dc1394_feature_set_mode(camera, DC1394_FEATURE_SHUTTER, DC1394_FEATURE_MODE_AUTO);
        }
        return;
    }
    
    // It's within range, set to manual if its not already
    if (mode == DC1394_FEATURE_MODE_AUTO) {
        dc1394_feature_set_mode(camera, DC1394_FEATURE_SHUTTER, DC1394_FEATURE_MODE_MANUAL);
    }
    
    // Set the value
    dc1394_feature_set_value(camera, DC1394_FEATURE_SHUTTER, shutter);
}

@end
