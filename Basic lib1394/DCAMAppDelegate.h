//
//  DCAMAppDelegate.h
//  Basic lib1394
//
//  Created by William Dillon on 7/9/12.
//  Copyright (c) 2012 William Dillon. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class DCAMDevice;
@class DCAMFrameView;

@interface DCAMAppDelegate : NSObject <NSApplicationDelegate>
{
    NSArray *deviceList;
    int _selectedDevice;
    DCAMDevice *device;
    
    float gain;
    float exposure;
    float shutter;
}

@property (assign) IBOutlet NSWindow *window;
@property (assign) IBOutlet DCAMFrameView *frameView;

@property (readonly)  NSArray *deviceList;
@property (readwrite) int selectedDevice;

@property (readwrite) float gain;
@property (readwrite) float exposure;
@property (readwrite) float shutter;
@property (assign) IBOutlet NSSlider *gainSlider;
@property (assign) IBOutlet NSSlider *exposureSlider;
@property (assign) IBOutlet NSSlider *shutterSlider;

- (IBAction)printFeatures:(id)sender;

@end
