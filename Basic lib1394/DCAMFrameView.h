//
//  DCAMFrameView.h
//  Basic lib1394
//
//  Created by William Dillon on 7/9/12.
//  Copyright (c) 2012 William Dillon. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "OpenGLController.h"
#import "DCAMAppDelegate.h"
//#import "ShaderProgram.h"

//#define TEXTURE_TYPE GL_TEXTURE_RECTANGLE_ARB
#define TEXTURE_TYPE GL_TEXTURE_2D

@interface DCAMFrameView : OpenGLController
{
    bool initialized;
    
    // This value sets the location of the line for current tuning
	float sliderValue;
    float tuningValue;
	float sampleRate;
    
    // These ivars maintain OpenGL state
	bool textureCurrent;
	unsigned int textureID;
    NSData *frameData;
    
//    ShaderProgram *shader;
    
    NSData *newSlice;    
}

@property (readwrite) IBOutlet DCAMAppDelegate *appDelegate;

@property (readonly) unsigned int textureID;

//- (void)initialize;
- (void)frameNotification:(NSNotificationCenter *)notification;

@end
