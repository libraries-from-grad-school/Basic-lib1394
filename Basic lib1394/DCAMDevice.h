//
//  DCAMDevice.h
//  Basic lib1394
//
//  Created by William Dillon on 7/9/12.
//  Copyright (c) 2012 William Dillon. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <dc1394/dc1394.h>

extern NSString *DCAMCapturedFrameNotification;

@interface DCAMDevice : NSObject
{
    dc1394camera_t *camera;
    
    int width, height, _iso;
    
    dispatch_queue_t FIFO_read_queue;
    bool running;
    
// Settings
    float _gain;
    float _brightness;
    float _shutter;
    float _exposure;
    
}

@property (readonly)  int width;
@property (readonly)  int height;
@property (readwrite) int iso;

// Camera limits
@property (readonly)  bool  gainFeature;
@property (readonly)  float minGain;
@property (readonly)  float maxGain;
@property (readonly)  bool  brightnessFeature;
@property (readonly)  float minBrightness;
@property (readonly)  float maxBrightness;
@property (readonly)  bool  shutterFeature;
@property (readonly)  float minShutter;
@property (readonly)  float maxShutter;
@property (readonly)  bool  exposureFeature;
@property (readonly)  float minExposure;
@property (readonly)  float maxExposure;

// Settings
@property (readwrite) float gain;
@property (readwrite) float brightness;
@property (readwrite) float shutter;
@property (readwrite) float exposure;

+ (NSArray *)deviceList;
+ (DCAMDevice *)newDeviceWithID:(int)index;

- (id)initWithID:(int)index;

- (bool)start;
- (void)stop;

- (void)printFeatures;

@end
