//
//  DCAMFrameView.m
//  Basic lib1394
//
//  Created by William Dillon on 7/9/12.
//  Copyright (c) 2012 William Dillon. All rights reserved.
//

#import "DCAMDevice.h"
#import "DCAMFrameView.h"
#import "OpenGLView.h"

@implementation DCAMFrameView

#pragma mark -
#pragma mark Init and bookkeeping methods
+ (NSOpenGLPixelFormat *)defaultPixelFormat
{
    NSOpenGLPixelFormatAttribute attributes [] = {
        NSOpenGLPFAWindow,
        NSOpenGLPFADoubleBuffer,
        NSOpenGLPFAAccumSize, 32,
        NSOpenGLPFADepthSize, 16,
        NSOpenGLPFAMultisample,
        NSOpenGLPFASampleBuffers, (NSOpenGLPixelFormatAttribute)1,
        NSOpenGLPFASamples, (NSOpenGLPixelFormatAttribute)4,
        (NSOpenGLPixelFormatAttribute)nil };
    
    return [[NSOpenGLPixelFormat alloc] initWithAttributes:attributes];
}

- (void)awakeFromNib
{
	[super awakeFromNib];
    
	textureCurrent = NO;
    
    // Subscribe to frame notifications
    NSNotificationCenter *center;
    center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(frameNotification:)
                   name:DCAMCapturedFrameNotification object:nil];
}

- (void)initGL
{
//    initialized = NO;
//    return;
//}
//
//-(void)initialize
//{
    // Clear any errors
    GLint error = glGetError();

    if (initialized) {
        return;
    } else {
        initialized = YES;
    }
    
    // Set black background
	glClearColor(0., 0., 0., 1.);
	
    // Set viewing mode
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-1.0, 1., -1.0, 1., -1., 1.);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
    
    // Set blending characteristics
//	glEnable(GL_BLEND);
//	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    // Set line width
	glLineWidth( 1.5 );
	
	glDisable(GL_DEPTH_TEST );
    glEnable( GL_TEXTURE_2D );
//    glEnable( GL_TEXTURE_RECTANGLE_ARB );

    // Check for errors
    error = glGetError();
    if (error != GL_NO_ERROR) {
        NSLog(@"Got an error from OpenGL: %d", error);
    }

    // Get a texture ID
	glGenTextures( 1, (GLuint*)&textureID );

    // Check for errors
    error = glGetError();
    if (error != GL_NO_ERROR) {
        NSLog(@"Got an error from OpenGL: %d", error);
    }
    
    // Set texturing parameters
	glBindTexture(  TEXTURE_TYPE, textureID );
	glTexParameteri(TEXTURE_TYPE, GL_TEXTURE_WRAP_S, GL_CLAMP );
	glTexParameteri(TEXTURE_TYPE, GL_TEXTURE_WRAP_T, GL_CLAMP );
	glTexParameteri(TEXTURE_TYPE, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(TEXTURE_TYPE, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // Check for errors
    error = glGetError();
    if (error != GL_NO_ERROR) {
        NSLog(@"Got an error from OpenGL: %d", error);
    }
    
    int width  = 1024;
    int height = 768;
    unsigned char *blankImage = malloc(4 * width * height * sizeof(float));
    //    bzero(blankImage, sizeof(blankImage));
    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            // Color cube
            blankImage[i*width*3 + j*3 + 0] = ((i % 10) < 5)? 1. : 0.;
            blankImage[i*width*3 + j*3 + 1] = ((j % 10) < 5)? 1. : 0.;
            blankImage[i*width*3 + j*3 + 2] = 0.;
            blankImage[i*width*3 + j*3 + 2] = 1.;
        }
    }
    
    glTexImage2D(TEXTURE_TYPE, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_FLOAT, blankImage);

    // Check for errors
    error = glGetError();
    if (error != GL_NO_ERROR) {
        NSLog(@"Got an error from OpenGL: %d", error);
    }

    free(blankImage);
    
    glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE );
    // Check for errors
    error = glGetError();
    if (error != GL_NO_ERROR) {
        NSLog(@"Got an error from OpenGL: %d", error);
    }

	glBindTexture(  TEXTURE_TYPE, 0 );
}

- (void)frameNotification:(NSNotification *)notification
{
    // Copy a reference to the new frame
    frameData = (NSData *)[notification object];

    // Redraw the display
    textureCurrent = NO;
    [super updateData:self];
}

- (void)draw
{
    // Clear any errors
    GLint error = glGetError();
   
    glClearColor(0., 0., 0., 1.);
    glClear(GL_COLOR_BUFFER_BIT);
    
    glBindTexture( TEXTURE_TYPE, textureID );

    // Check for errors
    error = glGetError();
    if (error != GL_NO_ERROR) {
        NSLog(@"Got an error from OpenGL: %d", error);
    }

    if (!textureCurrent && frameData != nil) {
        // Replace the oldest line in the texture
        const unsigned char *image = [frameData bytes];
        glTexImage2D(TEXTURE_TYPE, 0, GL_RGB, 1024, 768, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
        frameData = nil;
    }
    // Check for errors
    error = glGetError();
    if (error != GL_NO_ERROR) {
        NSLog(@"Got an error from OpenGL: %d", error);
    }

    glBegin( GL_QUADS ); {
		glColor4f( 0., 1., 0., 1. );
        
        glTexCoord2d( 0.,   1.);
		glVertex2f(  -1.,  -1.);
        
		glTexCoord2d( 0.,  0.);
		glVertex2f(  -1.,  1.);
        
		glTexCoord2d( 1.,  0.);
		glVertex2f(   1.,  1.);
        
		glTexCoord2d( 1.,  1.);
		glVertex2f(   1., -1.);
	} glEnd();
    
    glBindTexture( TEXTURE_TYPE, 0 );
	
    glFlush();
}

@end













































